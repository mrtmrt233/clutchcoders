package LC350;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by lipingzhang on 4/1/17.
 */
public class Solution2 {
    // What if the given array is already sorted? How would you optimize your algorithm?
    public int[] intersect(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        List<Integer> list = new ArrayList<>();

        for (int i = 0, j = 0; i < nums1.length && j < nums2.length; ) {
            if (nums1[i] == nums2[j]) {
                list.add(nums1[i]);
                i++;
                j++;
            }else if(nums1[i] < nums2[j]){
                i++;
            }else{
                j++;
            }
        }

        int[] ans = new int[list.size()];
        int i =0;
        for (int l : list){
            ans[i++] = l;
        }
        return ans;
    }
}
