class Solution(object):
        def intersect(self, nums1, nums2):
            """
            :type nums1: List[int]
            :type nums2: List[int]
            :rtype: List[int]
            """
            result = []
            for x in nums1[:]:
                if x in nums2:
                    result.append(x)
                    nums1.remove(x)
                    nums2.remove(x)
            return result

        ### follow up solution:
        # 1. what is the two list are already sort?
        # 2. what is one list is much shorter than the other one
        # 3. What if elements of nums2 are stored on disk, and the memory is limited such that you cannot load all elements into the memory at once?
